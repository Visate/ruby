// Search command
const ytdl = require("youtube-dl");
const services = {
  "yt": "ytsearch",
  "sc": "scsearch",
  "youtube": "ytsearch",
  "soundcloud": "scsearch"
};

exports.help = {
  name: "search",
  usage: "[service] [number of results] <search>",
  description: "Searches by default YouTube or SoundCloud for a song.",
  extendedhelp: "Searches by default YouTube or SoundCloud for a song. Defaults to 5 results if a number isn't provided. If a number is being specified, enclose the search in quotation marks. Only searches up to a maximum of 100 videos."
};

exports.config = {
  enabled: true,
  guildOnly: true,
  aliases: ["find"],
  permLevel: 0
};

exports.run = (client, msg, suffix) => {
  let index = 0;

  // Determining service to use, else default to YouTube
  let service = suffix.split(" ")[0];

  if (services[service]) {
    service = services[service];
    suffix = suffix.split(" ").slice(1).join(" ");
  }

  else service = "ytsearch";

  // Determining amount of videos to search for, defaults to 5
  let count = suffix.split(" ")[0];

  if (!isNaN(count)) {
    count = parseInt(count, 10);
    if (count < 1) return msg.channel.sendMessage("You can't search for less than one video!");
    if (count > 100) count = 100;
    suffix = suffix.split(" ").slice(1).join(" ");
  }

  else count = 5;

  let search = suffix;
  if (suffix.startsWith("\"") && suffix.endsWith("\"")) search = suffix.slice(1, -1);

  let collector = msg.channel.createCollector(m => m.author === msg.author, {time: 10000 * count});
  let currentVideo, lastMsg;
  let visible = false;

  collector.on("message", m => {
    if (!currentVideo || !lastMsg || !visible) return m.delete();
    if (m.content === "y") {
      m.delete();
      lastMsg.delete();
      collector.stop("queue");
    }

    else if (m.content === "n") {
      m.delete();
      lastMsg.delete();
      lastMsg = null;
      visible = false;
      index++;
      if (index === count) return collector.stop("finished");
    }

    else if (m.content === "exit") {
      m.delete();
      lastMsg.delete();
      collector.stop("exit");
    }
  });

  collector.on("end", (coll, reason) => {
    if (reason === "finished") msg.channel.sendMessage("Oh well~").then(m => m.delete(5000));
    else if (reason === "time") {
      lastMsg.delete();
      msg.channel.sendMessage("Time's up! Try again later~").then(m => m.delete(5000));
    }
    else if (reason === "queue") client.util.musicHandler.queueSong(msg, currentVideo.title, currentVideo.duration, currentVideo.thumbnail, currentVideo.webpage_url, currentVideo.webpage_url);
  })

  ytdl.getInfo(`${service}${count}:${search}`, (err, info) => {
    if (info[0]) {
      for (i in info) {
        currentVideo = info;
        msg.channel.sendEmbed(generateEmbed(client, msg, currentVideo))
        .then(m => {
          lastMsg = m;
          visible = true;
        });
      }
    }
  });
  // YouTube.searchVideos(search, count).then(videos => {
  //   if (!videos[0]) return msg.channel.sendMessage("No videos were found with this search!");
  //   let collector = msg.channel.createCollector(m => m.author === msg.author, {time: 10000 * count});
  //   let currentVideo;
  //   let lastMsg;
  //   let visible = false;
  //   YouTube.getVideoByID(videos[index].id).then(video => {
  //     currentVideo = video;
  //     lastMsg = msg.channel.sendEmbed({
  //       color: 3447003,
  //       author: {
  //         name: `${msg.author.username}#${msg.author.discriminator} (${msg.author.id})`,
  //         icon_url: msg.author.avatarURL
  //       },
  //       description: client.util.commonTags.stripIndents`
  //       **Result ${index + 1}/${count}:**
  //       [${video.title}](${video.url})
  //
  //       Is this the right video? \`y, n, exit\`
  //       `,
  //       image: {
  //         url: `https://img.youtube.com/vi/${video.id}/mqdefault.jpg`
  //       },
  //       footer: {
  //         text: "Music Search",
  //         icon_url: client.user.avatarURL
  //       }
  //     }).then(m => {
  //       lastMsg = m;
  //       visible = true;
  //     });
  //   });
  //   collector.on("message", m => {
  //     if (!currentVideo || !lastMsg || !visible) return m.delete();
  //     if (m.content === "y") {
  //       m.delete();
  //       lastMsg.delete();
  //       collector.stop("queue");
  //     }
  //
  //     else if (m.content === "n") {
  //       m.delete();
  //       lastMsg.delete();
  //       lastMsg = null;
  //       visible = false;
  //       index++;
  //       if (index === count) return collector.stop("finished");
  //       YouTube.getVideoByID(videos[index].id).then(video => {
  //         currentVideo = video;
  //         lastMsg = msg.channel.sendEmbed({
  //           color: 3447003,
  //           author: {
  //             name: `${msg.author.username}#${msg.author.discriminator} (${msg.author.id})`,
  //             icon_url: msg.author.avatarURL
  //           },
  //           description: client.util.commonTags.stripIndents`
  //           **Result ${index + 1}/${count}:**
  //           [${video.title}](${video.url})
  //
  //           Is this the right video? \`y, n, exit\`
  //           `,
  //           image: {
  //             url: `https://img.youtube.com/vi/${video.id}/mqdefault.jpg`
  //           },
  //           footer: {
  //             text: "Music Search",
  //             icon_url: client.user.avatarURL
  //           }
  //         }).then(m => {
  //           lastMsg = m;
  //           visible = true;
  //         });
  //       });
  //     }
  //
  //     else if (m.content === "exit") {
  //       m.delete();
  //       lastMsg.delete();
  //       collector.stop("exit");
  //     }
  //   });
  //
  //   collector.on("end", (collection, reason) => {
  //     if (reason === "finished") msg.channel.sendMessage("Oh well~").then(m => m.delete(5000));
  //     else if (reason === "time") {
  //       lastMsg.delete();
  //       msg.channel.sendMessage("Time's up! Try again later~").then(m => m.delete(5000));
  //     }
  //     else if (reason === "queue") {
  //       client.util.musicHandler.queueSong(msg, currentVideo.title, currentVideo.durationSeconds, `https://img.youtube.com/vi/${currentVideo.id}/mqdefault.jpg`, currentVideo.url, currentVideo.url, "youtube");
  //     }
  //   });
  // });
};

function generateEmbed(client, msg, video) {
  return {
    color: 3447003,
    author: {
      name: `${msg.author.username}#${msg.author.discriminator} (${msg.author.id})`,
      icon_url: msg.author.avatarURL
    },
    description: client.util.commonTags.stripIndents`
    **Result ${index + 1}/${count}:**
    [${video.title}](${video.webpage_url}) (${client.util.toHHMMSS(video.duration)})

    Is this the right video? \`y, n, exit\`
    `,
    image: {
      url: video.thumbnail
    },
    footer: {
      text: "Music Search",
      icon_url: client.user.avatarURL
    }
  }
}
