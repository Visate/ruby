const ytdl = require("youtube-dl");
const request = require("request");

exports.help = {
  name: "play",
  usage: "<URL|search>",
  description: "Adds a song to the queue. Can be video/music link, or a YouTube search.",
  extendedhelp: "Adds a song to the music queue, and if nothing is already playing, starts playback. You may send a video/music link or a search query which will search on YouTube."
};

exports.config = {
  enabled: true,
  guildOnly: true,
  aliases: ["p"],
  permLevel: 0
};

exports.run = (client, msg, suffix) => {
  let perms = client.util.checkPerms(msg);

  ytdl.getInfo(suffix, (err, info) => {
    if (err) ytdl.getInfo(`ytsearch:${suffix}`, (err, info) => {
      if (err) msg.channel.sendCode("x1", err);
      else client.util.musicHandler.queueSong(msg, info.title, info.duration, info.thumbnail, info.webpage_url, info.webpage_url);
      return;
    });

    if (info[0]) {
      for (i in info) {
        client.util.musicHandler.queueSong(msg, info[i].title, info[i].duration, info[i].thumbnail, info[i].webpage_url, info[i].webpage_url);
      }
    }

    else {
      client.util.musicHandler.queueSong(msg, info.title, info.duration, info.thumbnail, info.webpage_url, info.webpage_url);
    }
  })
};
